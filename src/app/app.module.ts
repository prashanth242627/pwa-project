import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { StoryEstimationComponent } from './story-estimation/story-estimation.component';
import { NewStoryEstimationComponent } from './story-estimation/new-story-estimation/new-story-estimation.component';
import { StoryEstimationService} from "./story-estimation/story-estimation.service";

@NgModule({
  declarations: [
    AppComponent,
    StoryEstimationComponent,
    NewStoryEstimationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [StoryEstimationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
