import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoryEstimationComponent } from './story-estimation.component';

describe('StoryEstimationComponent', () => {
  let component: StoryEstimationComponent;
  let fixture: ComponentFixture<StoryEstimationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StoryEstimationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoryEstimationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
