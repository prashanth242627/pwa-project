import { TestBed } from '@angular/core/testing';

import { StoryEstimationService } from './story-estimation.service';

describe('StoryEstimationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StoryEstimationService = TestBed.get(StoryEstimationService);
    expect(service).toBeTruthy();
  });
});
