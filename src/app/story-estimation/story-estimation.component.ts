import { Component, OnInit } from '@angular/core';
import { StoryEstimationService} from "./story-estimation.service"

@Component({
  selector: 'app-story-estimation',
  templateUrl: './story-estimation.component.html',
  styleUrls: ['./story-estimation.component.css']
})
export class StoryEstimationComponent implements OnInit {
  storyEstimations = [];

  constructor(private storyEstimationService: StoryEstimationService) {
    storyEstimationService.storyEstimationCreated$.subscribe(storyEstimation => {
      this.loadStoryEstimations();
    });
  }

  loadStoryEstimations() {
    this.storyEstimations = this.storyEstimationService.getList();
  }

  ngOnInit() {
    this.loadStoryEstimations();
    /*this.storyEstimations = this.storyEstimationService.getList();*/
  }

}
