import { Injectable, OnInit } from '@angular/core';
import { Subject} from "rxjs/index";

@Injectable({
  providedIn: 'root'
})
export class StoryEstimationService implements OnInit {

  private storyEstimationCreatedSource = new Subject<string>();

  storyEstimationCreated$ = this.storyEstimationCreatedSource.asObservable();
  storyEstimationsData = [];

  constructor() { }

  ngOnInit() {
    if(localStorage.getItem('storysizes') == null){
      this.storyEstimationsData = [];
    } else {
      this.storyEstimationsData = JSON.parse(localStorage.getItem('storysizes'));
    }
  }

  getList() {
    return JSON.parse(localStorage.getItem('storysizes'));
  }

  save(storyEstimation) {
    let storyEstimationsData = this.getList();
    console.log('ccccc' + this.storyEstimationsData);
    storyEstimationsData.push(storyEstimation);

    localStorage.setItem('storysizes', JSON.stringify(storyEstimationsData));
    this.storyEstimationCreatedSource.next(storyEstimation);
  }

}
