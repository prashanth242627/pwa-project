import { Component, OnInit } from '@angular/core';
import { StoryEstimation } from "../story-estimation";
import { StoryEstimationService} from "../story-estimation.service";

@Component({
  selector: 'app-new-story-estimation',
  templateUrl: './new-story-estimation.component.html',
  styleUrls: ['./new-story-estimation.component.css']
})
export class NewStoryEstimationComponent implements OnInit {

  storyEstimationModel = new StoryEstimation();

  constructor(private storyEstimationService: StoryEstimationService) {
  }

  ngOnInit() {
  }

  saveEstimation() {
    console.log('cameherre');
    console.log(this.storyEstimationModel);
    this.storyEstimationService.save(this.storyEstimationModel);
  }

}
