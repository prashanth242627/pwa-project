import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewStoryEstimationComponent } from './new-story-estimation.component';

describe('NewStoryEstimationComponent', () => {
  let component: NewStoryEstimationComponent;
  let fixture: ComponentFixture<NewStoryEstimationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewStoryEstimationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewStoryEstimationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
