export class StoryEstimation {
  constructor(public jiraId?: string,
              public userName?: string,
              public size?: string,
              public comment?: string) {}
}
